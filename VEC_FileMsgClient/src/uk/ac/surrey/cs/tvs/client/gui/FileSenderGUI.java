/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.client.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRootPane;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ClientException;
import uk.ac.surrey.cs.tvs.client.FileSender;
import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * FileSenderGUI provides a simple GUI for setting up a FileSender and sending arbritrary files to the MBB. Multiple files can be
 * selected, which will be packed into a zip file and sent. R
 * 
 * @author Chris Culnane
 * 
 */
public class FileSenderGUI {

  /**
   * Logger
   */
  protected static final Logger logger = LoggerFactory.getLogger(FileSenderGUI.class);

  /**
   * 
   * Launch the application.
   */
  public static void main(String[] args) {
    logger.info("FileSenderGUI instantiated directly - normally would expect it to be launched by FileSender");
    EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        try {
          FileSenderGUI window = new FileSenderGUI();
          window.frmFileSenderGui.setVisible(true);
        }
        catch (Exception e) {
          logger.error("Exception occurred during main", e);

        }
      }
    });
  }

  /**
   * Add File button
   */
  private JButton                btnAddFile;

  /**
   * Generate Keys button
   */
  private JButton                btnGenerateKeys;

  /**
   * Import certificate button
   */
  private JButton                btnImportCert;

  /**
   * Remove File button
   */
  private JButton                btnRemoveFile;

  /**
   * Save CSR button
   */
  private JButton                btnSaveCsr;

  /**
   * Underlying FileSender to use for sending the actual messages
   */
  private FileSender             fileSender;

  /**
   * DefaultListModel for the list of files to be submitted
   */
  private DefaultListModel<File> flm;

  /**
   * The root JFrame for the GUI
   */
  private JFrame                 frmFileSenderGui;

  /**
   * JList object that shows the list of files to send
   */
  private JList<File>            list;

  /**
   * Progress bar to show when sending to the MBB - initially hidden
   */
  private JProgressBar           sendProgressBar;

  /**
   * Default constructor DO NOT USE - this is only here to allow the WindowBuilder to construct the FileSenderGUI in the builder -
   * this should not be used for constructing it
   */
  public FileSenderGUI() {
    this.initialize();
  }

  /**
   * Constructor for creating a FileSenderGUI
   * 
   * @param config
   *          String path to config file to use
   * @throws JSONIOException
   * @throws IOException
   * @throws PeerSSLInitException
   * @throws MaxTimeoutExceeded
   */
  public FileSenderGUI(String config) throws JSONIOException, IOException, PeerSSLInitException, MaxTimeoutExceeded {
    this.initialize();
    // Create the underlying FileSender
    this.fileSender = new FileSender(config);

  }

  /**
   * Shows a JFileChooser and adds the selected files to the list of files to send
   */
  private void addFile() {
    final JFileChooser fc = new JFileChooser();
    fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fc.setMultiSelectionEnabled(true);
    if (fc.showOpenDialog(this.frmFileSenderGui) == JFileChooser.APPROVE_OPTION) {
      File[] selectedFiles = fc.getSelectedFiles();
      for (File f : selectedFiles) {
        logger.info("Adding file: {}", f);
        this.flm.addElement(f);
      }

    }
  }

  /**
   * Utility method for getting a reference to the application JFrame
   * 
   * @return JFrame for this application
   */
  public JFrame getFrame() {
    return this.frmFileSenderGui;
  }

  /**
   * Initialize the contents of the frame and creates all the components
   */
  private void initialize() {
    logger.info("Initialization started");
    this.frmFileSenderGui = new JFrame();
    this.frmFileSenderGui.setTitle("File Sender GUI");
    this.frmFileSenderGui.setBounds(100, 100, 600, 300);
    this.frmFileSenderGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JPanel panel_1 = new JPanel();
    this.frmFileSenderGui.getContentPane().add(panel_1, BorderLayout.NORTH);
    panel_1.setLayout(new BorderLayout(0, 0));

    JLabel lblNewLabel = new JLabel("File Sender");
    panel_1.add(lblNewLabel, BorderLayout.NORTH);
    lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);

    JPanel panel_2 = new JPanel();
    panel_1.add(panel_2);

    this.btnGenerateKeys = new JButton("Generate Keys");
    this.btnGenerateKeys.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        try {

          // This is currently performed on the UI thread - it is generally very quick, but could be moved to separate thread
          FileSenderGUI.this.fileSender.generateSigningKey();
          FileSenderGUI.this.updateStatus();
        }
        catch (ClientException e1) {
          logger.error("Exception whilst generating keys", e1);
          FileSenderGUI.this.showErrorMessage(e1);
        }
      }
    });
    panel_2.add(this.btnGenerateKeys);

    this.btnSaveCsr = new JButton("Save CSR");
    this.btnSaveCsr.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        FileSenderGUI.this.showCSRLocation();
      }
    });
    panel_2.add(this.btnSaveCsr);

    this.btnImportCert = new JButton("Import Cert");
    this.btnImportCert.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          FileSenderGUI.this.openCert();
          FileSenderGUI.this.updateStatus();
        }
        catch (IOException | ClientException e1) {
          logger.error("Exception whilst import certificate", e1);
          FileSenderGUI.this.showErrorMessage(e1);
        }

      }
    });
    panel_2.add(this.btnImportCert);

    JButton btnGetPublicKey = new JButton("Get Public Key");
    btnGetPublicKey.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        String publicKey;
        try {
          publicKey = FileSenderGUI.this.fileSender.getJSONPublicKey();
          FileSenderGUI.this.saveFile(publicKey);
        }
        catch (ClientException | IOException e1) {
          logger.error("Exception whilst getting the public key", e1);
          FileSenderGUI.this.showErrorMessage(e1);
        }

      }
    });
    panel_2.add(btnGetPublicKey);

    JPanel panel = new JPanel();
    this.frmFileSenderGui.getContentPane().add(panel, BorderLayout.CENTER);
    panel.setLayout(new BorderLayout(0, 0));

    JSplitPane splitPane = new JSplitPane();
    panel.add(splitPane, BorderLayout.CENTER);
    this.flm = new DefaultListModel<File>();
    this.list = new JList<File>(this.flm);
    this.list.setMinimumSize(new Dimension(200, 0));
    splitPane.setLeftComponent(this.list);

    JPanel panelRight = new JPanel();
    splitPane.setRightComponent(panelRight);
    panelRight.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

    this.btnAddFile = new JButton("Add File");
    this.btnAddFile.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        FileSenderGUI.this.addFile();
      }
    });
    panelRight.add(this.btnAddFile);

    this.btnRemoveFile = new JButton("Remove File");
    this.btnRemoveFile.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        FileSenderGUI.this.removeFile();
      }
    });
    panelRight.add(this.btnRemoveFile);

    JPanel panel_3 = new JPanel();
    this.frmFileSenderGui.getContentPane().add(panel_3, BorderLayout.SOUTH);
    panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));

    JButton btnSend = new JButton("Send");
    btnSend.setAlignmentX(Component.CENTER_ALIGNMENT);
    panel_3.add(btnSend);
    btnSend.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        try {
          FileSenderGUI.this.prepareFilesAndSend();
        }
        catch (ClientException | HeadlessException | JSONException e1) {
          logger.error("Exception during send", e1);
          FileSenderGUI.this.showErrorMessage(e1);
        }
      }
    });

    this.sendProgressBar = new JProgressBar();
    this.sendProgressBar.setMaximumSize(new Dimension(32767, 20));
    this.sendProgressBar.setMinimumSize(new Dimension(10, 20));
    this.sendProgressBar.setPreferredSize(new Dimension(148, 20));
    this.sendProgressBar.setStringPainted(true);
    this.sendProgressBar.setString("Sending to MBB");
    this.sendProgressBar.setVisible(false);
    this.sendProgressBar.setIndeterminate(true);
    panel_3.add(this.sendProgressBar);
    logger.info("Initialization finished");
  }

  /**
   * Launches the FileSenderGUI on the EDT
   */
  public void launch() {
    logger.info("Launch called on FileSenderGUI");
    EventQueue.invokeLater(new Runnable() {

      @Override
      public void run() {
        try {
          FileSenderGUI.this.updateStatus();
          FileSenderGUI.this.frmFileSenderGui.setVisible(true);
        }
        catch (Exception e) {
          logger.error("Exception during launch", e);
        }
      }
    });

  }

  /**
   * Shows a JFileChooser for the user to select where the CA signed certificate is. Then imports it into the FileSender client
   * config
   * 
   * @throws IOException
   * @throws ClientException
   */
  private void openCert() throws IOException, ClientException {
    final JFileChooser fc = new JFileChooser();
    FileNameExtensionFilter filter = new FileNameExtensionFilter("Certificate Files", "cert", "cert");
    fc.setFileFilter(filter);
    if (fc.showOpenDialog(this.frmFileSenderGui) == JFileChooser.APPROVE_OPTION) {
      this.fileSender.importSSLCertificate(fc.getSelectedFile().getAbsolutePath());
      logger.info("Importing certificate from: {}", fc.getSelectedFile());
    }
    else {
      logger.info("Cancel selected on open certificate dialog");
    }
  }

  /**
   * Asks the user for a description for this message and then shows GlassPane to block further UI access before preparing a List of
   * files to be submitted and calling the operation on a separate worker thread
   * 
   * @throws ClientException
   * @throws HeadlessException
   * @throws JSONException
   */
  private void prepareFilesAndSend() throws ClientException, HeadlessException, JSONException {
    logger.info("Prepating to send files");

    // Get the description
    String description = JOptionPane.showInputDialog(this.frmFileSenderGui, "Please enter a description for the submission",
        "Submission Description", JOptionPane.QUESTION_MESSAGE);

    if (description != null) {
      // Block the UI
      BlockingGlassPane glassPane = new BlockingGlassPane();
      JRootPane rootPane = SwingUtilities.getRootPane(this.frmFileSenderGui);
      rootPane.setGlassPane(glassPane);
      glassPane.showPane();

      // Show the progress bar and prepare the arrays
      this.sendProgressBar.setVisible(true);
      File[] filesArr = new File[this.flm.size()];
      this.flm.copyInto(filesArr);

      // Create the worker thread and execute
      SendFilesWorker<Void> sendFiles = new SendFilesWorker<Void>(this, this.fileSender, Arrays.asList(filesArr), description,
          this.sendProgressBar);
      sendFiles.execute();

    }

  }

  /**
   * Removes the selected file from the JList
   */
  private void removeFile() {
    if (this.list.getSelectedIndex() >= 0) {
      this.flm.removeElement(this.list.getSelectedValue());
    }
  }

  /**
   * Reset the JList to being empty
   */
  public void reset() {
    this.flm.clear();
  }

  /**
   * Saves the specified file into the location the user selects from a displayed JFileChooser
   * 
   * @param contents
   *          String of data to be saved
   * @throws IOException
   */
  private void saveFile(String contents) throws IOException {
    final JFileChooser fc = new JFileChooser();
    if (fc.showSaveDialog(this.frmFileSenderGui) == JFileChooser.APPROVE_OPTION) {
      logger.info("Saving file contents to {}", fc.getSelectedFile());
      IOUtils.writeStringToFile(contents, fc.getSelectedFile().getAbsolutePath());
    }
    else {
      logger.info("Cancel selected on save dialog");
    }
  }

  /**
   * Displays a dialog showing where the CSR is location - this should be sent to the CA for signing
   */
  private void showCSRLocation() {
    String csrLocation = this.fileSender.getCSRFileName();
    JOptionPane.showMessageDialog(this.frmFileSenderGui, "CSR is located at: " + csrLocation, "CSR Location",
        JOptionPane.INFORMATION_MESSAGE);

  }

  /**
   * Utility method for displaying an error dialog
   * 
   * @param e
   */
  private void showErrorMessage(Exception e) {
    JOptionPane.showMessageDialog(this.frmFileSenderGui, "The following error has occurred:\n" + e.getMessage(), "Error",
        JOptionPane.ERROR_MESSAGE);
  }

  /**
   * Gets the underlying status of the FileSender client and disables the buttons that are no longer relevant
   */
  private void updateStatus() {
    logger.info("Update status called: {}", this.fileSender.getNextTask());
    if (this.fileSender.isInitialised()) {
      this.btnGenerateKeys.setEnabled(false);
      this.btnSaveCsr.setEnabled(false);
      this.btnImportCert.setEnabled(false);
    }
    else {
      switch (this.fileSender.getNextTask()) {
        case "SigningKeyStore":
          this.btnGenerateKeys.setEnabled(true);
          this.btnSaveCsr.setEnabled(true);
          this.btnImportCert.setEnabled(true);
          break;
        case "SSLCSRLocation":
          this.btnGenerateKeys.setEnabled(false);
          this.btnSaveCsr.setEnabled(true);
          this.btnImportCert.setEnabled(true);
          break;
        case "CertificateLocation":
          this.btnGenerateKeys.setEnabled(false);
          this.btnSaveCsr.setEnabled(true);
          this.btnImportCert.setEnabled(true);
          break;
      }
    }
  }
}
